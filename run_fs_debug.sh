GEM5_BUILD=./
DEBUG_FLAGS="BranchTrace"
INSTRUCTIONS=3000000
declare -a POSITIONAL

for i in "$@"
do
case $i in
    -a|--analyze)
    ANALYZE=YES
    shift # past argument with no value
    ;;
    -i|--instructions)
    INSTRUCTIONS=$2
    shift
    shift # past argument with value
    ;;
    *)
	POSITIONAL+=($1)
          # unknown option
    shift
    ;;
esac
done


BM_NAME=${POSITIONAL[0]}
TAG=${POSITIONAL[*]:1}
TAG_NO_SPACE=$(echo $TAG | tr -d '\n' | tr -c '[:alnum:]-_' '_')
OUTFILENAME="$TAG_NO_SPACE"


./build/ARM/gem5.opt --outdir=m5out/$OUTFILENAME --debug-file=debug.txt --debug-flags=$DEBUG_FLAGS configs/example/fs.py  -r 1 --caches --l2cache --cpu-type=O3_ARM_v7a_3 --enable-context-switch-stats-dump 

if [[ -n ANALYZE ]]; then
	echo "Simultaion Complete: running analysis"
    ./analyze_trace.py ./m5out/$OUTFILENAME/debug.txt -o ./m5out/$OUTFILENAME -w 1000 -l 995 -t "${POSITIONAL[*]:1}" -b $BM_NAME
fi