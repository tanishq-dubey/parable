#!/bin/bash

for i in $(find ./se-benchmarks -type f -executable -exec basename {} \;); do
    ./run_se.sh $i $* &
    PIDS+=($!)
done

for p in $PIDS; do
    wait p
done
