﻿Parable
ECE 511 Final Project
==================
Tanishq Dubey, Rohan Mathur, Adam Auten
-------------------------------------------------------------

This repository is for simulating the Context Switch Accuracy Framework (CSAF).

----------

Building
------------

To enable CSAF, set the ``CONTEXT_SWITCH_LEARNING`` macro in
``src/cpu/pred/bpred_unit.hh``. Additional macros in that file allow
configuration of CSAF parameters. CSAF is turned on by default, but to compare
to baseline, this macro should be unset.

To build gem5 with CSAF, run
```
CPU_MODELS="AtomicSimpleCPU,MinorCPU,O3CPU,TimingSimpleCPU" scons build/ARM/gem5.opt
```
from the project root.

Running Simulations
------------------------------
To run simulations on multithreaded traces, use the ``launch_fs.py ``script.
Run ``launch_fs.py --help`` for more information.

Runscripts
----------------
Runscripts are provided in the ``runscripts/`` directory. These contain various
combinations of Stanford benchmarks run on the same linux kernel, which have
been build for ARMv7 and built into the filesystem image. These are selected by
``launch_fs.py`` to determine which benchmarks will be run simultaneously in a
context switched linux kernel.

Simulation Outputs
----------------------
When launch_fs.py is run, a simulation directory is generated in the m5out/
directory. The name of this directory is dependent on the runscript and tag
passed to launch_fs.py . 

In the sim directory, the ``system.tasks.prediction_rates.yaml`` file gives the
per-process aggregate misprediction rate across all time slices for which it
was scheduled. Additionally, in the ``m5out/`` directory, the instantaneous
misprediction rate during the simulation is saved to a JSON file whose name
matches that of the sim directory.

Plotting Misprediction Rate vs Time
-------------------------------------------
Upon a successful run of the ``launch_fs.py`` script, a JSON file is generated
in the m5out directory. This JSON file can be imported into the ipython
notebook under ``results/plotting.ipynb``. This notebook will open the JSON
files and generate the plots. The notebook also has a tool to generate the
differential branch trace plots shown in our paper.

Results
------------------
The simulation outputs and plots relevant to our project report are included in
the ``results/`` directory. This directory is broken down into two
subdirectories: ``results/plots/`` contains plots for our progress reports and
final report, while ``results/data/`` contains sim out directories for the runs
used in our report.


The methodDubey branch
----------------------------------
An alternate algorithm for CSAF was also explored, though the results were not
included in our final report. To build this version, simply checkout the
``methodDubey`` branch before building. Simulation and plotting proceeds as
usual.

