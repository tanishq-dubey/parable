#!/bin/bash
DEBUG_FLAGS="BranchTrace"
GEM5_BUILD=./
INSTRUCTIONS=3000000
declare -a POSITIONAL

for i in "$@"
do
case $i in
    -a|--analyze)
    ANALYZE=YES
    shift # past argument with no value
    ;;
    -i|--instructions)
    INSTRUCTIONS=$2
    shift
    shift # past argument with value
    ;;
    *)
	POSITIONAL+=($1)
          # unknown option
    shift
    ;;
esac
done

BM_NAME=${POSITIONAL[0]}
TAG=${POSITIONAL[*]:1}
TAG_NO_SPACE=$(echo $TAG | tr -d '\n' | tr -c '[:alnum:]-_' '_')
OUTFILENAME="$TAG_NO_SPACE-$BM_NAME"

BM=./se-benchmarks/$BM_NAME

if [[ ! -f $BM ]]; then
    echo "benchmark file $BM does not exist!"
    exit
fi
if  [[ -z BM_NAME ]]; then
	echo "Must specify a benchmark name."
	exit 
fi

$GEM5_BUILD/build/ARM/gem5.opt --outdir=m5out/$OUTFILENAME --debug-file=debug.txt --debug-flags=$DEBUG_FLAGS $GEM5_BUILD/configs/example/se.py -I $INSTRUCTIONS --caches --l2cache --cpu-type=O3_ARM_v7a_3 -c ./se-benchmarks/$BM_NAME

if [[ -n ANALYZE ]]; then
	echo "Simultaion Complete: running analysis"
    ./analyze_trace.py ./m5out/$OUTFILENAME/debug.txt -o ./m5out/$OUTFILENAME -w 1000 -l 995 -t "${POSITIONAL[*]:1}" -b $BM_NAME
fi





