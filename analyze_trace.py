#!/usr/bin/env python

"""A simple python script template.

"""

from __future__ import print_function
import os
import sys
import argparse
import csv
import json
from scipy import signal
import numpy as np
import re
#import progressbar

def moving_average(a, n):
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n-1:]/n

def main(arguments):

    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('input_trace', help="Input trace")
    parser.add_argument('-w', '--window-size', help="size of window",
                        default=1000, type=int)
    parser.add_argument('-l', '--overlap', help="overlap between windows",
        default=500, type=int);
    parser.add_argument('-o', '--out-base', help="Output file base")
    parser.add_argument('-b', '--benchmark', help="name of benchmark",
                        default='fs')
    parser.add_argument('-t', '--tag', help="tag the output json file",
                        default='')

    args = parser.parse_args(arguments)

    if(args.overlap >= args.window_size):
        print("overlap muxt be less than the window size\n.")
        return 0;

    with open(args.input_trace) as f:
        samples = [f for f in f.readlines() if re.match(r'\d+ \d+ \d', f)]
    samples = [[int(a) for a in x.strip().split()] for x in samples]

    win_begin = 0
    win_end = args.window_size
    max_sample = len(samples)
    mispred_accuracy = []
    #bar = progressbar.ProgressBar()

    np_stride = args.window_size - args.overlap;
    np_samples = np.array(samples);
    np_bp_samples = np_samples[:,2];
    #np_window = np.ones((args.window_size,))/args.window_size;
    #np_filtered = signal.fftconvolve(np_bp_samples,np_window, mode='valid');
    np_filtered = moving_average(np_bp_samples, args.window_size);
    np_filtered = np_filtered[0::np_stride];

    np_output = np.zeros((len(np_filtered), 4));
    np_output[:,0] = np.divide(np_samples[0::np_stride,0].astype(np.int), \
        1000)[0:len(np_filtered)];
    np_output[:,1] = np.arange(args.window_size-1, len(samples),\
        np_stride)[0:len(np_filtered)];
    np_output[:,2] = np_samples[0::np_stride,1][0:len(np_filtered)];
    np_output[:,3] = 1-np_filtered;

#    while win_end < max_sample:
#        #bar.update(100 * win_end/max_sample);
#        #if(args.outfile != sys.stdout):
#        sys.stdout.write("\r        \r{}".format(100 * win_end/max_sample))
#
#        accuracy = 0
#        for s in range(win_begin, win_end):
#            try:
#                accuracy += int(samples[s][2])
#            except:
#                pass
#
#        accuracy /= win_end-win_begin+1
#
#        mispred_accuracy.append(
#            [ int(samples[s][0])/1000, win_end, int(samples[s][1]),
#              1-accuracy] )
#
#        win_end += args.window_size - args.overlap
#        if(win_end - win_begin > args.window_size):
#           win_begin = win_end - args.window_size
#    print("");

    cols = ['cycle', 'conditional_branch_index', 'instruction_index', \
        'instantaneous_misprediction_rate']
    #args.outfile.write("cycle, conditional_branch_index, instruction_index, \
        #instantaneous_misprediction_rate\n")
    with open(args.out_base+".csv", "w") as f:
        f.write(','.join(cols))
        writer = csv.writer(f)
#        writer.writerows(mispred_accuracy)
        writer.writerows(np_output)

    json_dict = {};
#    json_dict['data'] = mispred_accuracy;
    json_dict['data'] = np_output.tolist();
    json_dict['cols'] = cols;
    json_dict['tag'] = args.tag;
    json_dict['magic'] = "Hi Jose :)"
    json_dict['benchmark'] = args.benchmark

    with open(args.out_base+ '.json', 'w') as fp:
        json.dump(json_dict, fp)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
