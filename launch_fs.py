#!/usr/bin/env python

"""A simple python script template.

"""

from __future__ import print_function
import os
import sys
import argparse
import csv
import json
#from scipy import signal
import subprocess
import signal
import process_stats
#from termcolor import colored
#import progressbar

# Runs a command in a child shell, printing stdout in realtime
# and forwarding SIGINT
def run_command(command):
    process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
    try:
        while True:
                # Check for output. If none and process has returned, break
                # otherwise, forward to stdout and flush stdout buffer
                output = process.stdout.read(1)
                if not output and process.poll() is not None:
                    break
                if output:
                    sys.stdout.write(output.decode('utf-8'))
                    sys.stdout.flush()
    # kill on any exception
    except:
        print(colored("\nGot CTRL-C... aborting.", 'yellow'));
        e = sys.exc_info()
        os.kill(process.pid, signal.SIGINT)
        while process.poll() is None:
            pass

    # get the subshell's exti code
    rc = process.poll()
    return rc


def colored(string, color):
    cmap = { 'header': '\033[96m',
             'white' : '\033[97m',
             'blue'  : '\033[94m',
             'green' : '\033[92m',
             'yellow': '\033[93m'}
    endcolor = '\033[0m'

    if color not in cmap.keys():
        return string;
    else:
        return cmap[color] + string + endcolor;


def main(arguments):
    # Parse Args
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('-w', '--window-size', help="size of window",
                        default=1000, type=int)
    parser.add_argument('-l', '--overlap', help="overlap between windows",
        default=995, type=int);
    parser.add_argument('-s', '--script', help="runscript path",
                        default=None)
    parser.add_argument('-u', '--ubuntu', action="store_true",
        help="run ubuntu image")
    parser.add_argument('--simple', action="store_true",
        help="run simple CPU model")
    parser.add_argument('-t', '--tag', help="tag the output json file",
                        default=None)
    parser.add_argument('-c', '--clock', help="system clock frequency",
                        default="1GHz")
    parser.add_argument('-I', '--instr-limit', help="instruction limit",
                        default=None, type=int)
    args = parser.parse_args(arguments)

    # set output directory based on runscript name and tag
    script_name = "noscript"
    if args.script is not None:
        script_name = os.path.basename(args.script).split('.')[0];
    outdir = "./m5out/"+script_name
    if args.tag is not None:
        outdir += '_'+args.tag

    # Print some sim info
    print(colored("Starting full system simulation launcher..", 'header'))
    print(colored("Sim output directory",'green')+
          ": {}".format(outdir))
    print(colored("Moving average window",'green')+
          ": {}".format(args.window_size))
    print(colored("Moving average overlap",'green')+
          ": {}".format(args.overlap))
    if args.script:
        print(colored("Using runscript",'green')+
              ": {}".format(args.script))
    else:
        print(colored("No runscript provided!", 'yellow'))
    if args.tag:
        print(colored("Simulation tag",'green')+
              ": {}".format(args.tag))
    else:
        print(colored("No simulation tag provided!", 'yellow'))
    if args.instr_limit is not None:
        print(colored("Instruction limit",'green')+
              ": {}".format(args.instr_limit))
    else:
        print(colored("Instruction limit",'green')+
              ": " + colored("None", 'yellow'))

    # create simlink of boot checkpoint in the output dir
    os.system('mkdir -p {}'.format(outdir))
    if args.ubuntu:
        checkpoint_path = "checkpoints/cpt.ubuntu-booted"
        disk_image = "aarch32-ubuntu-natty-headless.img"
        kernel_image = "vmlinux"
    else:
        checkpoint_path = "checkpoints/cpt.ael-booted-1ms"
        disk_image = "linux-aarch32-ael.img"
        kernel_image = "vmlinux-1ms"

    if args.simple:
        cpu_model = "AtomicSimpleCPU"
    else:
        cpu_model = "O3_ARM_v7a_3"

    # symlink the checkpoint into the sim directory
    os.system('ln -nfs ../../{} {}/cpt.0'.format(checkpoint_path, outdir))

    # print logging info to terminal
    print(colored("Using CPU model",'green')+": {}".format(cpu_model))
    print(colored("Using checkpoint",'green')+": {}".format(checkpoint_path))
    print(colored("Using disk image",'green')+": {}".format(disk_image))
    print(colored("Using kernel image",'green')+": {}".format(kernel_image))
    if args.simple:
        print(colored(
            "Note: no analysis will be run when using simple CPU",'yellow'))

    # Run GEM5
    cmd = "./build/ARM/gem5.opt "\
        "--debug-file=debug.txt "\
        "--debug-flags=BranchTrace "\
        "--outdir={} "\
        "configs/example/fs.py "\
        "-r 1 "\
        "--caches "\
        "--l2cache "\
        "--enable-context-switch-stats-dump ".format(outdir)
    cmd += "--kernel={} ".format(kernel_image)
    cmd += "--disk={} ".format(disk_image)
    cmd += "--cpu-type={} ".format(cpu_model)
    cmd += "--sys-clock={} ".format(args.clock)

    if args.script is not None:
        cmd += "--script={} ".format(args.script)
    if args.instr_limit is not None:
        cmd += "-I {} ".format(args.instr_limit)

    print(colored("Running simulation...", 'header'))
    run_command(cmd)
    print(colored("\nSimulation finished.", 'header'))

    # don't run analysis if using simple CPU
    if args.simple:
        return

    # process stats
    print(colored("Processing stats...", 'header'))
    process_stats.generate_task_stats(outdir)
    print(colored("\nProcessing finished.", 'header'))

    # Run analysis script
    cmd = "./analyze_trace.py "\
        "{}/debug.txt "\
        "-o {} "\
        "-w {} "\
        "-l {} "\
        "-t {} "\
        "-b {}".format( \
            outdir, outdir, args.window_size, args.overlap, \
            args.tag, script_name)
    print(colored("Running analysis...", 'header'))
    run_command(cmd)
    print(colored("\nAnalysis finished.", 'header'))


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))

