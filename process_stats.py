#!/bin/env python3
import re
import argparse
import sys
import yaml


def parse_stats(simdir):
    stats = simdir+"/stats.txt"
    tasks = simdir+"/system.tasks.txt"

    predicted = []
    incorrect = []

    for line in open(stats):
        match = re.search(r"branchPred\.condPredicted\s+(\d+)", line)
        if match:
           predicted.append(int(match.group(1)))
           continue
        match = re.search(r"branchPred\.condIncorrect\s+(\d+)", line)
        if match:
           incorrect.append(int(match.group(1)))
           continue

    assert len(predicted) == len(incorrect)

    # ignore first cpu context, as we don't know what it is from task.stats
    incorrect = incorrect[1:]
    predicted = predicted[1:]

    accuracies = []
    for i in range(0,len(predicted)):
        accuracies.append(incorrect[i]/predicted[i])

    
    contexts = []
    for line in open(tasks):
        match = re.search(r"next_tgid=(-?\d+)\s+next_task=(\w+)", line)
        if match:
           contexts.append((int(match.group(1)),str(match.group(2))))
        
    assert len(contexts) == len(accuracies)

    results = []
    for i in range(len(contexts)):
        results.append(
            (contexts[i][1], contexts[i][0], accuracies[i],
             incorrect[i], predicted[i]))
            

    return results


def generate_task_stats(sim_dir):
    results = parse_stats(sim_dir)

    incorrect = {}
    predicted = {}
    for a in results:
        name = "{}-{}".format(a[0],a[1])
        if name in incorrect:
            incorrect[name] += a[3]
            predicted[name] += a[4];
        else:
            incorrect[name] = a[3]
            predicted[name] = a[4];
            
    averages = {}
    for a in incorrect.keys():
        averages[a] = incorrect[a]/predicted[a]

    outfilename = sim_dir+"/system.tasks.prediction_rates.yaml"
    with open(outfilename, "w") as f:
        f.write(yaml.dump(averages, default_flow_style=False))

        

    #print(repr(accuracies))
    #print(len(accuracies))

def main(arguments):
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('sim_dir', help="sim directory")
    args = parser.parse_args(arguments)
    generate_task_stats(args.sim_dir)



if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))




